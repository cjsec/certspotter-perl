#!/usr/bin/perl
# Why did version go backwards?  I realized documentation
# was really lacking, esp for the module.  No docs = Not Done
use vars '$VERSION';
$VERSION = '0.9.1';

use strict;
use warnings;
use Getopt::Long;
use File::Basename qw/basename dirname/;
use Data::Dumper;
use Config::Simple ('-lc');

use Cwd  qw(abs_path);
use lib dirname (abs_path $0) . '/lib';
#use lib (dirname abs_path $0) . '/lib';
use CertSpotter;

my $scriptname = basename($0);

sub showhelp() {
	print "Syntax: $scriptname DomainName [--workdir DIRPATH] [--key APIkey] [ --config FILEPATH ] \\
                          [--match STRING] [--new] [--record] [--brief | --verbose] \\
                          [--force] [--record] [--expiring DAYS]

      Parameters:
        DomainName        Domain name to search on CertSpotter
        -c, --config      Path to configuration file. 
        -k, --key         Certspotter API key
                            Can be defined as APIKEY in config file instead.
        -m, --match       Limit results to certs with CN or any SAN containing STRING
                            Note, NAME is a simple search string, not a regex.	
        -w, --workdir     Directory to store some details of previous runs 
                            Can be defined as WORKDIR in config file instead.
	-x, --expiring    Limit results to certs expiring within DAYS days

      Options:
        -b, --brief       Display only cert subjects.  Cannot be used with --verbose
        -f, --force       Use with --record to force saving new max cert ID even when
                          it is a bad idea.  
        -n, --new         Only include certs since last time --record was used
                            (Requires --workdir)
        -r, --record      Record highest cert ID from certspotter so --new can exclude
	                    previous results.  Not recommended with --match or --expiring
	-s, --subs        Include sub-domains for the passed domain name
        -t, --test        Use test data instead of API call (avoid hitting certspotter 
                            during test/dev.)
        -v, --verbose     Show script progress and additional certificate detail. 
            --version     Display script version and exit

  Example:  (Recommended syntax for batch/automated monitoring of example.com domain.)
    $scriptname --config /etc/certsearch.conf --new --record --subs example.com 
	\n";
}

my $apikey='';
my $configfile='';
my $brief=0;
my $verbose=0;
my $show_help=0;
my $match='';
my $work_dir='';
my $newonly=0;
my $inclsubs=0;
my $record=0;
my $force=0;
my $expiring=0;
my $use_testdata=0;
my $showver=0;

# Allow bundled options such as -bns
Getopt::Long::Configure ("bundling");

my $valid_opts;
$valid_opts=GetOptions('config|c=s' => \$configfile,
	'workdir|w=s' => \$work_dir,
	'subs|s' => \$inclsubs,
	'brief|b' => \$brief,
	'key|k=s' => \$apikey,
	'verbose|v' => \$verbose,
	'match|m=s' => \$match,
	'new|n' => \$newonly,
	'force|f' => \$force,
	'record|r' => \$record,
	'test|t' => \$use_testdata,
	'expiring|x=i' => \$expiring,
	'version' => \$showver,
        'help|h' => \$show_help );

if(!($valid_opts) or $show_help) {
	showhelp() ;
	exit (!($valid_opts));
}

if ($showver) {
	print "Version: v$VERSION\n";
	exit 0;
}

if ($verbose and $brief) {
	warn "ERROR: --brief cannot be used with --verbose\n";
	exit 1;
}

if ($record) {
	if ($match or $expiring or not $inclsubs) {
		if ($force) {
			print "--force used to enable unrecommended use of --record\n" if ($verbose);
		}
		else {
			die qq/
WARNING!: --record is meant for batch\\automated monitoring of CT logs.

--record should not be used with --match or --expiring and you should use 
the --subs option when using --record as well.

Being inconsistent with these options can cause newly issued certificates
to be overlooked when using this script to monitor CT logs as a security measure.

However, if you will always use the same options every time for this domain
you can use --force to override this safety measure.\n/;
		}
	}
}

my $domain;
if ($use_testdata) {
	# If using test data, domain is gnu.org
	$domain = 'gnu.org';
	print "Using gnu.org test data. Not making CertSpotter API call!\n";
}
else {
	if (exists($ARGV[0])) {
	$domain = $ARGV[0];
	print "Search domain passed on command line: $domain\n" if ($verbose);
	}
	else {
		die "ERROR: You must pass a domain name as an argument.\n";
	}
}

if ($apikey) {
	print "API KEY (command line): $apikey\n" if ($verbose);
}

if ($work_dir) {
	print "Work directory (command line): $work_dir\n" if ($verbose);
}

my %config;
if ($configfile) {
	print "Configuration file: $configfile\n" if ($verbose);
	unless (-f $configfile ) {
		die "Unable to find configuration file: $configfile\n";
	}
	open (CONF, "<$configfile") or die "Unable to open $configfile for read.\n";
	close CONF;
	Config::Simple->import_from($configfile, \%config);
	if ($config{'default.apikey'}) {
		if ($apikey) {
			print "APIKEY in config file superceded by command line.\n" if ($verbose);
		}
		else {
			$apikey=$config{'default.apikey'};
			print "API KEY (config file): $apikey\n" if ($verbose);
		}
	}
	if ($config{'default.workdir'}) {
		if ($work_dir) {
			print "WORKDIR in config file superceded by command line.\n" if ($verbose);
		}
		else {
			$work_dir=$config{'default.workdir'};
			print "Work Directory (config file): $work_dir\n" if ($verbose);
		}
	}
}

if (($newonly or $record) and not $work_dir) {
	warn "ERROR: --new and --record require a working directory\n";
	exit 1;
}


# Set expiration notice days default to 30, but use --expiring value if passed
my $xdays = $expiring ? $expiring : 30;

my $lineno=0;
my $domcount=0;
my %domains;
if (-d $work_dir and -w $work_dir) {
	print "$work_dir is a directory and is writable.\n" if ($verbose);
	print "Opening $work_dir/csdomains\n" if ($verbose);
	unless (open (DOMLIST, "<$work_dir/csdomains") ) {
		warn "Unable to open $work_dir/csdomains for read.\n";
		warn "All certificates will be retrieved.\n";
	}
	else {
		my $line;
		print "Parsing domain work list\n" if ($verbose);
		while($line=<DOMLIST>) {
			$lineno++;
			# Skip if comment
			if ($line =~ /^\s*#/) {
				next;
			}
			# Strip CR and LF
			$line =~ s/[\r\n]//g;
			print qq/(Line $lineno) "$line"\n/ if ($verbose);
			# strip everything that isn't valid in a domain name except "=" 
			$line =~ s/[^A-Za-z0-9\.=]//g;
			my ($dom,$maxid) = split(/=/, $line);
			# Make sure max ID # is numeric
			$maxid += 0;
			$domains{$dom} = $maxid;
			print "  ##Domain $dom with last seen certspotter ID of $maxid\n" if ($verbose);
			$domcount++;
		}
		close DOMLIST;
		print "## Found $domcount domains in csdomains file.\n" if ($verbose);
	}
}
else {
	if ($work_dir) {
		warn "$work_dir must be a writable directory. Search positions will not be updated.\n";
		$work_dir='';
	}
}

# clean passed domain name if needed
my $searchdomain = lc($domain);
# Limit to valid domain characters
$searchdomain =~ s/[^0-9a-zA-Z\-\.\*]//g;
# Remove any leading/trailing dots
$searchdomain =~ s/^\.+//;
$searchdomain =~ s/\.+$//;
if ($verbose and $domain ne $searchdomain) {
	print "Search domain cleaned from '$domain'\n   to '$searchdomain'\n";
}

# If --match was used, get rid of characters that can't be part of a domain name
# and escape "." 
if ($match) {
	print "Original --match string = '$match'\n" if ($verbose);
 	$match =~ s/[^0-9a-zA-Z-\.]//g;
	print "Cleaned --match string = '$match'\n" if ($verbose);
	$match =~ s/\./\\./g;
}

if ($verbose) {
	print "Additional CL options:\n";
	print "  Include sub-domains (-s): " ;
	print $inclsubs ? "YES\n" : "NO\n";
	print "  Brief listing (-b): " ;
	print $brief ? "YES\n" : "NO\n";
	print "  Only list newly found certs (-n): " ;
	print $newonly ? "YES\n" : "NO\n";
	print "  Show only domains matching REGEX: /$match/\n";
	print "  Show expiring domains: " ;
	print $expiring ? "YES (within $expiring days)\n" : "NO\n";	
}


my $error=0;
my $lastid=0;

# If we are asking only for new records do we have a starting point?
if ($newonly) {
	# Did we find a matching domain in work file?
	if (exists($domains{$searchdomain})) {
		$lastid=$domains{$searchdomain};
		# Do we need to limit results based on that finding?
		if ($newonly) {
			print "Using last CS ID of $lastid for search.\n" if ($verbose);
		}
	}
	else {
		print "No matching record found to use for --new. Requesting all records.\n" if ($verbose) ;
	}
}

my $cs = CertSpotter->new({testdata=>$use_testdata,
			domain=>$searchdomain,
			apikey=>$apikey,
			subs=>$inclsubs,
			lastid=>$lastid,
			match=>$match,
			expiring=>$expiring,
			verbose=>$verbose
		});

if ($cs->error()) {
	print $cs->error() . "\n";
	exit 2; }

if ($brief) {
	# Called with --brief just show Subject and exit
	my $subject;
	for $subject (sort $cs->all_subjects()) {
		print "Subject: $subject\n";
	}
	exit 0;
}

# If certs matching our query were found
if ($cs->count() == 0) {
	print "No certificates found.\n" if ($verbose);
	exit 0;
}

if ($newonly) {
	print "New TLS certificates found for $searchdomain\n";
	print "  (or a subdomain of $searchdomain)\n\n";
}

# Produce regular report for each returned cert
my $notfirst=0;
my $id;
foreach $id (@{$cs->id_list}) {
	print "-------------------------------\n" if ($notfirst);
	$notfirst = 1;

	my $expmsg = $cs->will_expire() ? " !!EXPIRING in <$xdays days!!" : '';
	printf "%15s: %d\n", 'ID',$id;
	printf "%15s: %s\n", 'Subject', $cs->subject($id);
	printf "%15s: %s\n", 'Not Before', $cs->not_before($id);
	printf "%15s: %s%s\n", 'Not After', $cs->not_after($id), $expmsg;
	my $san;
	print "Alternate Names:\n";
	foreach $san ($cs->sans($id)) {
		print "            $san\n" ;
	}
	printf "%15s: %s\n", 'CA Issuer', $cs->issuer($id);
	# Report extra detail if --verbose
	if ($verbose) {
		printf "%15s: %s\n", 'email', $cs->email($id);
		printf "%15s: %s\n", 'MD5-FP', $cs->fingerprint_md5($id);
		printf "%15s: %s\n", 'SHA256-FP', $cs->fingerprint_sha256($id);
		# Does anyone really want this?
		#printf "%15s: %s\n", 'Modules', $cs->modulus($id);
	}
}

printf "Newest certificate ID: %d\n", $cs->max_id() if ($verbose);

if ($record and $lastid < $cs->max_id ) {
	# Was a new ID found?  If so, update and write out a new file
	print "Need to update csdomains due to --record\n" if ($verbose);
	unless (open (DOMLIST, ">$work_dir/csdomains") ) {
		warn "Unable to open $work_dir/csdomains for write.\n";
		warn "Cannot record new search results.\n";
	}
	else {
		print "Writing new state file:\n" if ($verbose);
		print DOMLIST "# Created/updated by $scriptname\n";
		$domains{$searchdomain} = $cs->max_id;
		my $outcount=0;
		my $key;
		foreach $key (sort keys %domains) {
			$outcount++;
			print DOMLIST "$key=" . $domains{$key} . "\n";
			print "  $key=" . $domains{$key} . "\n" if ($verbose);
		}
		print "Wrote $outcount domain records.\n" if ($verbose);
		close DOMLIST;
	}
}

