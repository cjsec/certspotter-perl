package CertSpotter;

use Crypt::OpenSSL::X509;
use MIME::Base64;
use WWW::Curl::Easy;
use JSON;
use Data::Dumper;

sub new{
	my ($class,$args) = @_;
	my $self = bless { testdata => $args->{testdata},
			domain => $args->{domain},
			apikey => $args->{apikey},
			verbose => $args->{verbose},
			subs => $args->{subs},
			lastid => $args->{lastid},
			match => $args->{match},
			expiring => $args->{expiring},
			json => 'Standin JSON data'
			}, $class;
	$self->_fetchApiData();
	unless ($self->{error}) {
		$self->_parseData();
	}

	return $self;
}

sub error{
	my $self = shift;
	return $self->{error};
}

sub id_list{
	my $self = shift;
	return [sort keys %{$self->{certs}}];
}	

sub count{
	# When someone asks for count, they likely want to
	# know how many matching certs were found
	my $self = shift;
	return $self->{matches};
}

sub api_count{
	# If no filters (--match, --expiring) this should
	# be the same as count()
	my $self = shift;
	return $self->{count};
}

sub max_id{
	my $self = shift;
	return $self->{maxid};
}

sub subject{
	my $self = shift;
	my $_cid = shift;
	return $self->{certs}{$_cid}{subject};
}
sub sans{
	my $self = shift;
	my $_cid = shift;
	return @{$self->{certs}{$_cid}{sans}};
}

sub not_before{
	my $self = shift;
	my $_cid = shift;
	return $self->{certs}{$_cid}{notbefore};
}

sub not_after{
	my $self = shift;
	my $_cid = shift;
	return $self->{certs}{$_cid}{notafter};
}

sub will_expire{
	my $self = shift;
	my $_cid = shift;
	return $self->{certs}{$_cid}{willexpire};
}

sub issuer{
	my $self = shift;
	my $_cid = shift;
	return $self->{certs}{$_cid}{issuer};
}

sub email{
	my $self = shift;
	my $_cid = shift;
	return $self->{certs}{$_cid}{email};
}

sub fingerprint_md5{
	my $self = shift;
	my $_cid = shift;
	return $self->{certs}{$_cid}{fingerprint_md5};
}

sub fingerprint_sha256{
	my $self = shift;
	my $_cid = shift;
	return $self->{certs}{$_cid}{fingerprint_sha256};
}

sub modulus{
	my $self = shift;
	my $_cid = shift;
	return $self->{certs}{$_cid}{modulus};
}

sub all_subjects{
	my $self = shift;
	my $_id;
	my $_subjects=[];
	foreach $_id (@{$self->id_list()}) {
		push @$_subjects, $self->subject($_id);
	}
	return @$_subjects;
}

sub rawdata{
	my $self = shift;
	return $self->{csdata};
}

sub json{
	my $self = shift;
	return $self->{json};
}

sub url{
	my $self = shift;

	my $_url = 'https://api.certspotter.com/v1/issuances?domain=';
	$_url .= $self->{domain} ;
	$_url .= '&include_subdomains=' ;
	$_url .= $self->{subs} ? 'TRUE' : 'FALSE';
	$_url .= '&expand=cert&expand=dns_names';
	if ($self->{lastid}) {
		$_url .= '&after=' . $self->{lastid};
	}
	return $_url;
}

sub _fetchApiData{
	my $self = shift;

	$self->{error} = '';
	if ($self->{testdata}) {
		$self->{json} = _testdata();
	}
	else {
		my $_apiUrl = $self->url();
		print "\nAPI URL=$_apiUrl\n" if ($self->{verbose});

		# Perform the API call
		my $_curl = WWW::Curl::Easy->new;
		if ($self->{apikey}) {
			$_curl->setopt(CURLOPT_USERNAME, $self->{apikey});
		}
		else {
			print "No API key provided!\n" if ($self->{verbose});
		}
		$_curl->setopt(CURLOPT_HEADER,0);
		$_curl->setopt(CURLOPT_URL, $_apiUrl);
		 
		my $_response_body;
		$_curl->setopt(CURLOPT_WRITEDATA,\$_response_body);

		my $_retcode=0;
		print "Starting API call\n" if ($self->{verbose});
		# Perform the actual request
		$_retcode = $_curl->perform;
		if ($_retcode == 0) {
			# We received data from API
			print "Response received from API call\n" if ($self->{verbose});
			$self->{json} = $_response_body;
		}
		else {
			# Error code, type of error, error message
			$self->{error} = "Error curl call failed: $_retcode ".$_curl->strerror($retcode)." ".$_curl->errbuf ;
			warn("\n".$self->{error}."\n") if ($self->{verbose});
		}

	}

	unless ($self->{error}) {
		my $_jdata;
		$_jdata = from_json($self->{json});

		# An ARRAY is returned for a normal successful call
		# If we got a HASH, then something went wrong and hash should contain the error msg
		if (ref($_jdata) eq 'HASH') {
			$self->{error} = "ERROR: Call to CertSpotter API failed.\n";
			$self->{error} .= "Message: " . $_jdata->{'message'} ;
			warn $self->{error} if ($self->{verbose});
		}
		else {
			$self->{csdata} = $_jdata ;
		}
	}
}

sub _parseData{
	my $self = shift;
	$self->{apicount} = 0;
	$self->{matches} = 0;
	$self->{maxid} = 0;

	my $_xdays = $self->{expiring} ? $self->{expiring} : 30;
	print " Identify certs which expire within $_xdays days.\n" if ($self->{verbose});

	my $_rec;
	foreach $_rec (@{$self->{csdata}}) {
		$self->{apicount}++;
		# Convert cert from base64 text
		my $_dercert=decode_base64($_rec->{'cert'}{'data'});
		# Pull the cert info into an object
		my $_x509 = Crypt::OpenSSL::X509->new_from_string(
			  $_dercert, Crypt::OpenSSL::X509::FORMAT_ASN1);
		# Now we have access to all the info we need
		
		my $_subject = $_x509->subject();
		if ($_subject =~ /CN=([^ ,]+)/ ) {
			# We only save the CN value
			$_subject = $1;
		}
		print "** Certificate #$_count: $_subject\n" if ($self->{verbose});

		my $_curcert = {};
		my $_id = $_rec->{'id'};
		$_curcert->{subject} = $_subject;
		$_curcert->{sans} = $_rec->{'dns_names'};

		# Check --match if specified
		if ($self->{match}) {
			my @_matches = grep {/$self->{match}/} @{$_curcert->{sans}};
			unless ((scalar @_matches > 0) or $_subject =~ /$_match/) {
				print " Skipping because of --match\n" if ($self->{verbose});
				next;
			}
		}
		my $_willexpire = $_x509->checkend($_xdays*86400);
		$_curcert->{willexpire} = $_willexpire;

		# Skip if matching for expiring certs but not expired
		unless (!($self->{expiring}) or $_willexpire) {
			print " Skipping because not expiring in --expires days\n" if ($self->{verbose});
			next;
		}
		$self->{matches}++;
		if ($_id > $self->{maxid}) {
			$self->{maxid} = $_id;
		}
		$_curcert->{notafter} = $_x509->notAfter();
		$_curcert->{notbefore} = $_x509->notBefore();
		$_curcert->{cert} = $_rec->{'cert'}{'data'};
		$_curcert->{issuer} = $_x509->issuer();
		$_curcert->{email} = $_x509->email();
		$_curcert->{modulus} = $_x509->modulus();
		$_curcert->{fingerprint_md5} = $_x509->fingerprint_md5();
		# Deal with slightly older Crypt::OpenSSL::X509
		if ($_x509->can("fingerprint_sha256")) {
			$_curcert->{fingerprint_sha256} = $_x509->fingerprint_sha256();
		}
		else {
			$_curcert->{fingerprint_sha256} = "Unavailable";
		}
		$self->{certs}{$_id} = $_curcert;
	}
	print "Received ". $self->{apicount} ." certificates.\n" if ($self->{verbose});
	print "Kept ". $self->{matches} ." certificates.\n" if ($self->{verbose});
}

sub _testdata() {
	# Test data could have been large JSON string output from API.
	#
	# Using standard perl data structure so it can be modified more
	# easily, then converting to JSON.
	# 
	# Suggested TO-DOs: 
	#    Break out to own module to get it out of main script
	#
	#    Consider generating fake certs - Don't consider this worth
	#    the effort for general testing though.
	my $_response_data = [
          {
            'pubkey_sha256' => 'f6fab50e3eb3e36a3afd342a1f3485630684b40dd98c58d7c712f3b96c0d226d',
            'dns_names' => [
                             'elpa.gnu.org'
                           ],
            'cert' => {
                        'sha256' => '2509b085d014bb7b40e45b458b650ce7187f76beaf9f7dd300ffa3fcc0c6101d',
                        'type' => 'cert',
                        'data' => 'MIIFUTCCBDmgAwIBAgISAxi/9o3oVz/otUpZiqafPLRjMA0GCSqGSIb3DQEBCwUAMEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQDExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMzAeFw0yMDEwMDIxMDU0MjJaFw0yMDEyMzExMDU0MjJaMBcxFTATBgNVBAMTDGVscGEuZ251Lm9yZzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMtyCRCDvexfHzslzhp2ea4H9pJR2KhtrYOxnN9KOwJ8rQI49XOvfDnpRWxx6itfuTkkfMZnt20h+7ULE+Xxn+tnu5fnzsrJ40dRMDeYoc9z8iPwEOdyaRK0VkcPEd24uJnyt9gv3lVFmI8z9sYMw7i1a1Rnt3R/i+HhaMrEvxrHRVaDAi1y+T5TGFgccdGuF+1kBQWv7IVyyjm5376DIrzGAFqmmzjRDfrRzxT3O+MukJmbNSuHcKJczfO+79Nbf9F93JQYlsaWAItGh8lU7vylBl7nIx5pKBoGT2Fffk9pSIqo3ou+M7ZpY464Z/mAdZV1+s1xs1VvFdnQzUG9iIUCAwEAAaOCAmIwggJeMA4GA1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUC8OdOYpcqHO7YPSu/rmYhdPZJI8wHwYDVR0jBBgwFoAUqEpqYwR93brm0Tm3pkVl7/Oo7KEwbwYIKwYBBQUHAQEEYzBhMC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcC5pbnQteDMubGV0c2VuY3J5cHQub3JnMC8GCCsGAQUFBzAChiNodHRwOi8vY2VydC5pbnQteDMubGV0c2VuY3J5cHQub3JnLzAXBgNVHREEEDAOggxlbHBhLmdudS5vcmcwTAYDVR0gBEUwQzAIBgZngQwBAgEwNwYLKwYBBAGC3xMBAQEwKDAmBggrBgEFBQcCARYaaHR0cDovL2Nwcy5sZXRzZW5jcnlwdC5vcmcwggEFBgorBgEEAdZ5AgQCBIH2BIHzAPEAdgBvU3asMfAxGdiZAKRRFf93FRwR2QLBACkGjbIImjfZEwAAAXTpKcL7AAAEAwBHMEUCICXBKo+wom87u109ixLJpe7+t93mFOKFxl0A4426tr95AiEA+aNOGeI7iZ8w55W65axexUJS4f5xmnKmAKuK91w//kQAdwAHt1wb5X1o//Gwxh0jFce65ld8V5S3au68YToaadOiHAAAAXTpKcM4AAAEAwBIMEYCIQD2eZUgTTu/O0vqs5ptbmGEd/5zpgtlDoLK+3c3yPvRTQIhANSCwz9LrQbc/RWFTnqilioOcF1NItqPQAya8ulwUU6dMA0GCSqGSIb3DQEBCwUAA4IBAQCM+hVyyQzbflvcoiK1uSL+E5KpUU/mmUHMUtuuAzjo55MrdOVv4RbouSed0FbDzBfw9FuaJYvQEE2giErgR4TaDfG7++QvEfD6GgcKgIjIpYKJ/PYE6RPwN97GQYFGsj+p+CaVjpO7TWuwnnvAD2DAGYE9x+PnUewjUDLob96PBZpc2U0paP6c8CDp8jYnY0S/Ie37/oO0+ZWp5t6uxc1DEfZrj/INkxX9t0Nt/l0lDzuFiEoFqbzY0hpzY66kuizFWvza5B8+DArmO3ewBHdzqrI7GvR0/x4cGFfr9AAB0fVZKgvGQNua6vm0Hxg2CkVqGJcb0xKwSLKPer/BU0Oz'
                      },
            'issuer' => {
                          'pubkey_sha256' => '60b87575447dcba2a36b7d11ac09fb24a9db406fee12d2cc90180517616e8a18',
                          'name' => 'C=US, O=Let\'s Encrypt, CN=Let\'s Encrypt Authority X3'
                        },
            'not_before' => '2020-10-02T10:54:22-00:00',
            'tbs_sha256' => 'd05bf64f07406459c06f08c4ea60accfcf69b43aa79547d68c039f8c9a297a32',
            'id' => '1966965354',
            'not_after' => '2020-12-31T10:54:22-00:00'
          },
          {
            'id' => '1968033772',
            'not_after' => '2020-12-31T23:04:21-00:00',
            'tbs_sha256' => '3f2f4771b080c4206b60e828636ef7c650fba8d468d5e71c5d8e8aff1389581b',
            'not_before' => '2020-10-02T23:04:21-00:00',
            'issuer' => {
                          'pubkey_sha256' => '60b87575447dcba2a36b7d11ac09fb24a9db406fee12d2cc90180517616e8a18',
                          'name' => 'C=US, O=Let\'s Encrypt, CN=Let\'s Encrypt Authority X3'
                        },
            'cert' => {
                        'sha256' => '8f57e26b511dd71dd848e425aaf52ef157213b8bac026cdcf6713ca6ebcaab39',
                        'type' => 'cert',
                        'data' => 'MIIFWzCCBEOgAwIBAgISA8pQWtdyYNWvjU06xBweRb2FMA0GCSqGSIb3DQEBCwUAMEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQDExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMzAeFw0yMDEwMDIyMzA0MjFaFw0yMDEyMzEyMzA0MjFaMBYxFDASBgNVBAMTC2Z0cC5nbnUub3JnMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx1mk2muaYECLBU+1l+/uWTmF9AC161m4I0v3WYvXvCyHpBTDJ9FKiMNVlkfGwxrEv2PBNQsuriglI+3uJ1jbIS/2mdwsXGrDJJrIab+Vq1isNAhEo346upvbsAqxtgV5YsgTaVJffGHyRGuyKFglONhVU+FeQ+uRnV301lfWNllW81JljaJl0acYe+w2i1bdRJP9zQeIrBquE0YVTHAJA4bhgPPyeg9xjSdVM8/CrwStXuC0le3SWTo/QuK+Dlb9osTXP9ik9d1/9PkWbi/Yd5qRNAN7EmkCe1Wu1huyKGBdSmJ/TUTirFQqg/EXL2xXXMB+1+Xa7YtRHlGeJ8jjJQIDAQABo4ICbTCCAmkwDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBQx0JSsAfHgDpx7d2N+QYM9y9OQTzAfBgNVHSMEGDAWgBSoSmpjBH3duubRObemRWXv86jsoTBvBggrBgEFBQcBAQRjMGEwLgYIKwYBBQUHMAGGImh0dHA6Ly9vY3NwLmludC14My5sZXRzZW5jcnlwdC5vcmcwLwYIKwYBBQUHMAKGI2h0dHA6Ly9jZXJ0LmludC14My5sZXRzZW5jcnlwdC5vcmcvMCUGA1UdEQQeMByCDWFscGhhLmdudS5vcmeCC2Z0cC5nbnUub3JnMEwGA1UdIARFMEMwCAYGZ4EMAQIBMDcGCysGAQQBgt8TAQEBMCgwJgYIKwYBBQUHAgEWGmh0dHA6Ly9jcHMubGV0c2VuY3J5cHQub3JnMIIBAgYKKwYBBAHWeQIEAgSB8wSB8ADuAHUAb1N2rDHwMRnYmQCkURX/dxUcEdkCwQApBo2yCJo32RMAAAF068YWaQAABAMARjBEAiB7sz8Uk+MA+1fHFTQBetfV/7/lRShkN1p4vzOS5PYeKAIgTuIuVpL7mqAW3dpfm/dEejJXTPXRkwMTNS4ERyQ9SS8AdQAHt1wb5X1o//Gwxh0jFce65ld8V5S3au68YToaadOiHAAAAXTrxhahAAAEAwBGMEQCIFCUb9TotAXUDUmN29Yv5vikgfoglOp3krca2GNhvOWmAiBpuV7d3H5+UUbj5rGwexg7POcDADNsUJqCzxa7q0EcyDANBgkqhkiG9w0BAQsFAAOCAQEAimOg9UJMjcyqflfq2rU6LoA9urxmrXL2yVb/YCjxiS0m+FzAeyo/K3yFVR6veh/5jCzu3GLzewNWxYJvsWrtSQzeDRJTv8zY5b8ndFIqximT5pfslSRK33jaF359fouZRl9C4UfruvVXi9ljipob8PCJpup90wnshVFRzC32fDnWCosWHO0F4IvCzwChuTdvqCUUCTH+ljWo6ER1hzhZC5I9RzVs8006P3PUWwlg1gy4QBIGgOtkOzFbz5lK4PctqkZ6cQ4m4ChR2HrG6vLKDRLpQ6S8zbUqFctf2Z9MYWs8c8YSxPGFyMFebp3s2esDrzMhR6Qq3yKrc9N2Dc29MQ=='
                      },
            'dns_names' => [
                             'alpha.gnu.org',
                             'ftp.gnu.org'
                           ],
            'pubkey_sha256' => '0bdd1d4d49cf739a78896c102b3fcf7742370a85e8f2792f8619a48d2bb4780f'
          },
          {
            'not_after' => '2021-01-02T09:26:46-00:00',
            'id' => '1971020617',
            'tbs_sha256' => '4e92e3a55b176fa10f23cc29932dea065552adbfc1ccc71f4da50eff49f11b43',
            'cert' => {
                        'type' => 'cert',
                        'sha256' => 'f4488c5f75eeee2a30cb9ac7932efe0ecadebe1ec779f8eed4449886d52ab883',
                        'data' => 'MIIHCDCCBfCgAwIBAgISA1wWc7C+eVznpB7LFKTuUswBMA0GCSqGSIb3DQEBCwUAMEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQDExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMzAeFw0yMDEwMDQwOTI2NDZaFw0yMTAxMDIwOTI2NDZaMCQxIjAgBgNVBAMTGWRvd25sb2FkLnNhdmFubmFoLmdudS5vcmcwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQDJaEFZz+IuwUT/fCm1cD+BXnFrJO45jobqxr/ICivRVjTjT/tiuE2P8KkGVDMsaM3n/MSp1gkqOxhJ7jleu2nZqHv5jWfC9RcR0L0x+xdUV/VKAwOhfIOGRCkLXFp+OUaeNKsDyLj+D/GKzevrIuGj8bxkZvsYYpIp/PM+XXuiirJkuRGu2P1pBLoepgHqAxzX9M6cTybnNb3LIYo081jb2DmqgPTO13UKUMQn3VJ4viTnJBNuSBWldWhcBGjsocQNwpgaOZ+zzBGLreSy8zdPdznq0/T7+XKcDJNG31+5bh4bmjizFA3L6JpqJvt105GzrqUOWOyslBC4qxLLtOhFABg1TlEhrdz9Dp06a34zXDuFx+zDj8j9wN5G8hM5WE4cofrvz4Pjx+KTMK49RctWqNoLdJINamBNcAD0flHyVlLjIejtzAGQihYkyNE/r5/fQwDxNy/4caHt6Adkep2c16r+UdFQ0ztaKPZ4pFO8y27AiSjkffO6YVIdEKiiXbwtqm9ygz+qXqtqLWnKyc6wy6KFJmzWs/tbf7K3ovE77wvwgTfQwS5FnbhjMPr0MyXZ/EHiyNbySnK8YkDmLAnoDzE6hnik84MSkfoiYfUO4mvlSYOevmKPhFfMdJQ483iPmvKqoY8fIw9K8wI+BjjOPIP3qy8+0t1sQVc46TgOuwIDAQABo4IDDDCCAwgwDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBTwyD35QvYTfIkYB/6vg/WAk9GAEzAfBgNVHSMEGDAWgBSoSmpjBH3duubRObemRWXv86jsoTBvBggrBgEFBQcBAQRjMGEwLgYIKwYBBQUHMAGGImh0dHA6Ly9vY3NwLmludC14My5sZXRzZW5jcnlwdC5vcmcwLwYIKwYBBQUHMAKGI2h0dHA6Ly9jZXJ0LmludC14My5sZXRzZW5jcnlwdC5vcmcvMIG/BgNVHREEgbcwgbSCE2RsLnNhdmFubmFoLmdudS5vcmeCFmRsLnNhdmFubmFoLm5vbmdudS5vcmeCDWRsLnN2LmdudS5vcmeCEGRsLnN2Lm5vbmdudS5vcmeCGWRvd25sb2FkLnNhdmFubmFoLmdudS5vcmeCHGRvd25sb2FkLnNhdmFubmFoLm5vbmdudS5vcmeCE2Rvd25sb2FkLnN2LmdudS5vcmeCFmRvd25sb2FkLnN2Lm5vbmdudS5vcmcwTAYDVR0gBEUwQzAIBgZngQwBAgEwNwYLKwYBBAGC3xMBAQEwKDAmBggrBgEFBQcCARYaaHR0cDovL2Nwcy5sZXRzZW5jcnlwdC5vcmcwggEGBgorBgEEAdZ5AgQCBIH3BIH0APIAdwCUILwejtWNbIhzH4KLIiwN0dpNXmxPlD1h204vWE2iwgAAAXTzJkgDAAAEAwBIMEYCIQC0my4U7xKwZAKZYV7iWy42J9xaxViOuu3hGJnTfZQQpwIhAK9cQ/LNIW/O4Wxfvvc3Gx8GRuD42yfciYLFwufhxmztAHcAfT7y+I//iFVoJMLAyp5SiXkrxQ54CX8uapdomX4i8NcAAAF08yZIQwAABAMASDBGAiEA31bu8BtaSC6p856ZQVJEsOVfZQ1GrnKntzbXb8YZuJYCIQDs5MW81fkIIoLB3eotPNJWxje4wYj+1jQH3e55zooMLDANBgkqhkiG9w0BAQsFAAOCAQEAaDO1BHZKV5/tTq6msLFzXADpYPdiF1J+0slGwSgDYlyVrhi7YA8uVUR3KRpVfo9Ngm33l/FO9KY+wvNYLtSESjNeqxVvFNrXFHCTJBvsYysCR0jHNcQ1ECo2HvqlT/L6Jq/PKQAJw3EKCkR89GmNupRl0qqqjYwO0kCrrq41X9ubDNM6FGM73aTo+TiKMBtRHDyRiq2W76k/BMttXNzgZ9bR0SJQvXAIl9GPaFvwIQNz0fR2Lz8sL1MAHzuhdT2r2JugS9vP7ciXfJUYhAvF2XfuyW+fefAcoF5fRVcJBMWxyxME23LzliXVzw1i01a9u6sV9Lst5WT28EfweJPDHQ=='
                      },
            'dns_names' => [
                             'dl.savannah.gnu.org',
                             'dl.savannah.nongnu.org',
                             'dl.sv.gnu.org',
                             'dl.sv.nongnu.org',
                             'download.savannah.gnu.org',
                             'download.savannah.nongnu.org',
                             'download.sv.gnu.org',
                             'download.sv.nongnu.org'
                           ],
            'pubkey_sha256' => '7569e93a3709a726d8965979dd5c2d68d92199b130d2b7f7617de7ae3c98f2e1',
            'not_before' => '2020-10-04T09:26:46-00:00',
            'issuer' => {
                          'name' => 'C=US, O=Let\'s Encrypt, CN=Let\'s Encrypt Authority X3',
                          'pubkey_sha256' => '60b87575447dcba2a36b7d11ac09fb24a9db406fee12d2cc90180517616e8a18'
                        }
          },
          {
            'cert' => {
                        'sha256' => 'b3b60cf0bbc35cf5f8d317167a7e7835b3cdcdfcb9e6326493b8e9ca9d143793',
                        'type' => 'cert',
                        'data' => 'MIIFTzCCBDegAwIBAgISA4vJUpqsQTsnm++XUgtidmc0MA0GCSqGSIb3DQEBCwUAMEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQDExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMzAeFw0yMDEwMDQyMTA0MjFaFw0yMTAxMDIyMTA0MjFaMBcxFTATBgNVBAMTDGd1aXguZ251Lm9yZzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMYS58Bv+tt5IO7zwoqSLDFHjyz1F8R0wgA7OP9KPcPFbKalrpr91KGUNY86wzNe+gGg3JHu/cfz/mEFrEEY9VGG1RuNbZDjyRe34uhU99PQY6sz0Ydf0x4Rh27Bh5XZ/ZxC32yanl06kSL0YsLg0988rJHiaatYMuPp+09Qbs+OGnL6sGYwkgPUUujyayfmtYoSsNaxUNVAZne7Xqs7fLFjK9FA54AOilBqcyRf2SxFdYGmC1KOvJ+bWKjiz5Vdonblv5hBiCArEEvwFJX3gMiC42dwx33aaMGBWKBmo8pa8FYn9f7l4PAP+xnHBUR6iQ2JnVVVJBfrLNiRSfMdvU8CAwEAAaOCAmAwggJcMA4GA1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUs1xBQRaNsUEKKgXpDBTXcLaDi1UwHwYDVR0jBBgwFoAUqEpqYwR93brm0Tm3pkVl7/Oo7KEwbwYIKwYBBQUHAQEEYzBhMC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcC5pbnQteDMubGV0c2VuY3J5cHQub3JnMC8GCCsGAQUFBzAChiNodHRwOi8vY2VydC5pbnQteDMubGV0c2VuY3J5cHQub3JnLzAXBgNVHREEEDAOggxndWl4LmdudS5vcmcwTAYDVR0gBEUwQzAIBgZngQwBAgEwNwYLKwYBBAGC3xMBAQEwKDAmBggrBgEFBQcCARYaaHR0cDovL2Nwcy5sZXRzZW5jcnlwdC5vcmcwggEDBgorBgEEAdZ5AgQCBIH0BIHxAO8AdgBvU3asMfAxGdiZAKRRFf93FRwR2QLBACkGjbIImjfZEwAAAXT1pPIOAAAEAwBHMEUCIFlUasGz4a3I7hPg5Ee28rWl58eEEQaNIkiWcrLg7nZdAiEA8EaWcp+6Tvr2c7hNf4C4KAdekjKo4uKlP9itBsfxkZ8AdQD2XJQv0XcwIhRUGAgwlFaO400TGTO/3wwvIAvMTvFk4wAAAXT1pPHgAAAEAwBGMEQCIARxV2c37dwzIsysCAjzwkhTfucPV4C6F+babeC6wKnEAiAHoaGrHxwcDLfQo6oJfliWdAmnwhkO8HnHsHMFQwzRdzANBgkqhkiG9w0BAQsFAAOCAQEAFZRl0ErhaVRHgEvxwtE8dYwLjPmEbRxBAjqRDvVPpD0C9UHxA1mRx1bR7OpTZxACBvUDzX5rx3hTwEqNTib9+4sye5z4r9zQG+V9nWXM0K1MMEQ4MhmciRiJXDvrxfMylkAQ089GVEvku70BEAL0CtWxxTXFlX2bCje605zQIVzASQCWaFwNrQmObUcoNNUchbFZI8xDTfWknq8repJQkUi9N1uKs6S302xcs0UcrF5IxU7rgFJZCUP9GfnXRPGZM3gV8KZMOqBpkbycp9XRcvvvcSzG5GBc/u3xVSGjxTv6NZnEouAnIbuXONso5tNwpk8YzaLQMtfDoMdDzwEALw=='
                      },
            'pubkey_sha256' => '55a472b403aa5b1a8efbb32deaae343e21a085ec25bdcfbc788af390b7288f3a',
            'dns_names' => [
                             'guix.gnu.org'
                           ],
            'issuer' => {
                          'name' => 'C=US, O=Let\'s Encrypt, CN=Let\'s Encrypt Authority X3',
                          'pubkey_sha256' => '60b87575447dcba2a36b7d11ac09fb24a9db406fee12d2cc90180517616e8a18'
                        },
            'not_before' => '2020-10-04T21:04:21-00:00',
            'tbs_sha256' => '98b448a5554842c1e2eb2941fc4f37a480a1cf7fbba34282b357e11ec87a9fd9',
            'not_after' => '2021-01-02T21:04:21-00:00',
            'id' => '1971945818'
          },
          {
            'tbs_sha256' => 'b86c2ea95aebd8a02ac89d1ed6332999b1e5b6694684fb37fa014984a988cbfa',
            'not_after' => '2021-01-02T21:08:54-00:00',
            'id' => '1971953829',
            'issuer' => {
                          'pubkey_sha256' => '60b87575447dcba2a36b7d11ac09fb24a9db406fee12d2cc90180517616e8a18',
                          'name' => 'C=US, O=Let\'s Encrypt, CN=Let\'s Encrypt Authority X3'
                        },
            'not_before' => '2020-10-04T21:08:54-00:00',
            'pubkey_sha256' => '4a61934c02529d7d9f716f7eea2e1ea2fd94a2f7e341abbfd355eae202df395d',
            'dns_names' => [
                             'bayfront.guix.gnu.org',
                             'bayfront.guix.info',
                             'guix-hpc.bordeaux.inria.fr',
                             'hpc.guix.info'
                           ],
            'cert' => {
                        'type' => 'cert',
                        'sha256' => 'ce1cf50eb0cc7ed73c21b538175c50d0e4eb56e149b91927171530834d710064',
                        'data' => 'MIIFoDCCBIigAwIBAgISBL9MqIaVh6jZdaGN0ovAa/nXMA0GCSqGSIb3DQEBCwUAMEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQDExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMzAeFw0yMDEwMDQyMTA4NTRaFw0yMTAxMDIyMTA4NTRaMCAxHjAcBgNVBAMTFWJheWZyb250Lmd1aXguZ251Lm9yZzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALzRCxPjQ+HiTpDDN60LzqNUZRkyt/eLHIa9TEOxZl/hJUxvxI3YwALgZpzu/lkwtyrTPDBVPHq6ME4R1+AXcm7EApQ5Yko8XCe9s/wHeqx+YoLu8X4gjW0eEYbaqGS5ty9eOuOpLJfd9RPSL2u8UoZCHTpqAK+8ZA5G6sgzZwPnE+AbiBI+0xif7x8UXgVtT0pWLnn63TVFDS3NYlBXXnK7AwtR+mMBccPfKzyM47GIIfuNq1XE43WCPU7yxKfEattX2h2t0DEmzgBr8pa/IrX2i9whp/AxbHMd9mls2ApYaobL+3ximzyVcWJkwGs0HWEUMIWSCQQWEuq9p+ERLKUCAwEAAaOCAqgwggKkMA4GA1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUutFpDqcozK3IeQ8ZygpSw6VnlTgwHwYDVR0jBBgwFoAUqEpqYwR93brm0Tm3pkVl7/Oo7KEwbwYIKwYBBQUHAQEEYzBhMC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcC5pbnQteDMubGV0c2VuY3J5cHQub3JnMC8GCCsGAQUFBzAChiNodHRwOi8vY2VydC5pbnQteDMubGV0c2VuY3J5cHQub3JnLzBfBgNVHREEWDBWghViYXlmcm9udC5ndWl4LmdudS5vcmeCEmJheWZyb250Lmd1aXguaW5mb4IaZ3VpeC1ocGMuYm9yZGVhdXguaW5yaWEuZnKCDWhwYy5ndWl4LmluZm8wTAYDVR0gBEUwQzAIBgZngQwBAgEwNwYLKwYBBAGC3xMBAQEwKDAmBggrBgEFBQcCARYaaHR0cDovL2Nwcy5sZXRzZW5jcnlwdC5vcmcwggEDBgorBgEEAdZ5AgQCBIH0BIHxAO8AdQD2XJQv0XcwIhRUGAgwlFaO400TGTO/3wwvIAvMTvFk4wAAAXT1qR0MAAAEAwBGMEQCICHIeOJ8QLvl7dUkNGu0K/DONE7fDstHBwlLshrUjC58AiAEJXvlZcFvHhYYFmPOy83lnHSlHRnP/4HeTDqi4e346AB2AESUZS6w7s6vxEAH2Kj+KMDa5oK+2MsxtT/TM5a1toGoAAABdPWpHS8AAAQDAEcwRQIhAI9auQnAamTYticlwPkzcpEvn6Mze8+PsXmdEGl/0kTDAiAeZAWg0zaH+fDbrb0L75ECg6fB1jlHtcRzNRU/1e/BPTANBgkqhkiG9w0BAQsFAAOCAQEAM9hQwJBSs58xOgC9xBguU1BRQvgI/DLbZKP5E2/yAEEQbhCXzN1KUppaqlT3wO1fse+4z5ym5zgsppHnRTu5pYjyfICgAAHFSw9x7MXtoaYdCR2uY7Dw39PA9qtmSqkWpzZtbrH+9Q1UBzCupeBjbuvM2p0rLH0na69LovhKKFmvzHHS85Id50Nsd/Cx03LNjQvCzfTgczLcrQlAKYtnWvnHh2suXkSGb6X6yckjSsDZ+upF8MLkX6+Gqx8Om/Rbe9VOWcc16x9YFFlqkpBMXDdnaNV8vkWwgJosIPSGI06HCH+QtHWBClpL4B271j9NjoYLApBYvs+L8Pn12WL94w=='
                      }
          }
        ];
	return (to_json($_response_data));
}

1;
